# README #

NPZ scroller is a tool to more efficiently analyze maps from TFT printers 

### How do I get set up and use the script? ###

* Navigate to a destination folder in terminal and run <br /><code>git clone https://kevinc_26@bitbucket.org/kevinc_26/npz_scroller.git</code>
* Follow the instructions in <code>opener.bat</code> to set up the .NPZ association, and point the opener to the right directory to run <code>print_scroller.py</code>
* To run the script:
  * Attempt to open any .npz file. A terminal will open, and the dialog box will appear
  * Select a 'maps', 'stackup', or 'maps/[layer_n]' (e.g. maps/3) folder
  * Only specify the 'type' for a 'maps' folder - layer folders and stackup folders will automatically pull up all the .npz information
  * The script will load all the appropriate data, and pull up an interactive plot
  * Scroll the mouse wheel to navigate
* On selecting a folder:
  * The first time you use it upon restarting your computer, the initial directory will be the home directory. It will be annoying to click all the way back to the .npz you are looking at
  * After using once, it will set the last directory used as the initial directory
    * This makes analysis of multiple folders easier
  

### Who do I talk to? ###

* Kevin