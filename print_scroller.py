"""IMPORT LIBRARIES"""
import numpy as np
import matplotlib.pyplot as plt
from skimage.color import rgb2gray, rgba2rgb
import glob
from scroller_menu import gui_menu
from npz_scroller import npz_scroll
import os

"""
def crop_image(a, border=0):
    ## TODO: Integrate this into display
    coords = np.argwhere(a)
    x_min, y_min = coords.min(axis=0)
    x_max, y_max = coords.max(axis=0)

    x_min = max([0, x_min - border])
    x_max = min([np.shape(a)[0], x_max + 1 + border])
    y_min = max([0, y_min - border])
    y_max = min([np.shape(a)[1],y_max + 1 + border])
    return a[x_min:x_max, y_min:y_max]
"""

if __name__ == '__main__':
    """UI SELECT"""
    print(os.path)
    mapFlag, stackFlag, npzFlag = 0, 0, 0
    outputs = gui_menu(os.path)
    if outputs['folder'][-4:] == 'maps':
        mapFlag = 1
        imCounter = len(glob.glob('{}/*_{}.png'.format(outputs['folder'], outputs['type'])))
    elif outputs['folder'][-8:] == 'stackups':
        stackFlag = 1
        imCounter = len(glob.glob('{}/*_layer_active_stackups.gif'.format(outputs['folder'])))
    elif outputs['folder'][-1:].isnumeric():
        npzFlag=1
    else:
        raise ValueError('Folder must be a maps or stackup folder to view slices')

    """LOAD INTO 3D ARRAYS"""
    if npzFlag:
        npz_scroll(outputs['folder'])
    else:
        if mapFlag:
            scroll_matrix = np.zeros((480, 512, imCounter))
        elif stackFlag:
            path = '{}/1_layer_active_stackups.gif'.format(outputs['folder'])
            lx, ly = rgb2gray(rgba2rgb(plt.imread(path))).shape
            scroll_matrix = np.zeros((lx, ly, imCounter))
        else:
            raise ValueError('Wrong folder type - must be /maps, /stackups, or /(n_layer)')

        for i in range(1, imCounter + 1):
            if mapFlag:
                path = outputs['folder'] + '/' + str(i) + '_' + outputs['type'] + '.png'
            else:
                path = outputs['folder'] + '/' + str(i) + '_layer_active_stackups.gif'
            scroll_matrix[:, :, i - 1] = rgb2gray(rgba2rgb(plt.imread(path))) * 255
        print(scroll_matrix.shape)

        """MATPLOTLIB VISUALIZE"""


        # see https://github.com/jttoombs/CAL-Python/blob/master/Python/display_functions.py for reference

        class IndexTracker(object):
            def __init__(self, X):
                if X.ndim != 3:
                    raise ValueError('Input volume must have 3 dimensions. Use view_plot for 2D images.')
                self.ax = ax
                self.slice_index = 2
                self.title = outputs['folder'][-10:] + ', ' + outputs['type']
                ax.set_title('%s\nUse scroll wheel to navigate volume' % self.title)

                self.X = X
                self.n0, self.n1, self.n2 = X.shape

                maxval = np.max(X)

                self.ind = self.n2 // 2
                self.im = ax.imshow(self.X[:, :, self.ind], vmin=0, vmax=200, cmap='inferno')

                self.update()

            def onscroll(self, event):
                # print("%s %s" % (event.button, event.step))
                if self.slice_index == 0:
                    num_slices = self.n0
                elif self.slice_index == 1:
                    num_slices = self.n1
                elif self.slice_index == 2:
                    num_slices = self.n2

                if event.button == 'up':
                    self.ind = (self.ind + 1) % num_slices
                else:
                    self.ind = (self.ind - 1) % num_slices
                self.update()

            def update(self):
                if self.slice_index == 0:
                    self.im.set_data(self.X[self.ind, :, :])
                elif self.slice_index == 1:
                    self.im.set_data(self.X[:, self.ind, :])
                elif self.slice_index == 2:
                    self.im.set_data(self.X[:, :, self.ind])
                ax.set_title('%s\nSlice: %s' % (self.title, self.ind))

                self.im.axes.figure.canvas.draw()


        fig, ax = plt.subplots(1, 1)

        tracker = IndexTracker(scroll_matrix)

        fig.canvas.mpl_connect('scroll_event', tracker.onscroll)
        plt.show()
