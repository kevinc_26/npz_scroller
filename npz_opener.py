#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
from skimage import morphology

plt.ion()


def crop_image(a, b='a', border=0):
    if isinstance(b, str):
        b = a
    coords = np.argwhere(b)
    x_min, y_min = coords.min(axis=0)
    x_max, y_max = coords.max(axis=0)

    x_min = max([0, x_min - border])
    x_max = min([np.shape(a)[0], x_max + 1 + border])
    y_min = max([0, y_min - border])
    y_max = min([np.shape(a)[1],y_max + 1 + border])
    return a[x_min:x_max, y_min:y_max]


def subplot_layout(num):
    # Quick way to build reasonable grid of subplots
    short = int(np.floor(np.sqrt(num)))
    long = int(np.ceil(num / short))
    fig, axs = plt.subplots(short, long, sharex=True, sharey=True)
    return fig, axs


def plot_dict(data, suptitle=None, vmin=None, vmax=None, crop='target', plot_generators=()):
    if isinstance(data, np.ndarray):
        data = [data]
    if isinstance(data, (list, tuple)):
        data = dict(enumerate(data))
    original_data = data
    data = {k: v for k, v in data.items() if len(np.shape(v)) == 2}

    fig, axs = subplot_layout(len(data) + len(plot_generators))
    if suptitle is not None:
        fig.suptitle(suptitle)

    if not isinstance(axs, np.ndarray):
        axs = np.array([axs])

    # Plot each map.
    axs = axs.ravel()
    for i, (name, arr) in enumerate(data.items()):
        ax = axs[i]
        plot_arr(**locals())
    for i, plot_generator in enumerate(plot_generators):
        plot_generator(axs[len(data) + i], original_data)


def plot_arr(ax, arr, name='', data={}, vmin=0, vmax=None, crop='target', **garbage):
    if len(np.shape(arr)) == 2:
        if np.max(arr) == 1:
            arr = arr.astype('bool')
        cmap = ['inferno', 'gray'][1 * (arr.dtype == np.dtype('bool'))]
        vmax = 200#[vmax, np.max(arr)][1 * (vmax is None)]
        if np.amax(arr) <= 1:
            vmax = 1
        vmin = [vmin, np.min(arr)][1 * (vmin is None)]
        img = np.fliplr(arr)
        if 0:
            if isinstance(crop, str) and crop in data:
                img = crop_image(img, np.fliplr(data[crop]), border=2)
            else:
                img = crop_image(img, border=2)

        ax.imshow(img, cmap=cmap, vmin=vmin, vmax=vmax, origin='lower')
        ax.set_title(name)


def hist(ax, arr, title='Histogram of map'):
    map_distribution = arr.flatten()
    map_distribution = map_distribution[map_distribution != 0]
    shax = ax.get_shared_x_axes()
    shax.remove(ax)
    shay = ax.get_shared_y_axes()
    shay.remove(ax)
    ax.hist(map_distribution, bins=np.arange(0,300))
    ax.set_title(title)


def maximize_plots():
    manager = plt.get_current_fig_manager()
    try:
        manager.window.state('zoomed')
    except:
        manager.window.showMaximized()


def load(fn, arr_generator=lambda d: d, excluded_keys=(), only_keys=None, excluded_keywords=()):
    print('a')
    npz = np.load(fn)
    print(npz.files)
    npz = {k: npz[k] for k in npz.files}
    print(npz.keys())
    npz = arr_generator(npz)
    if only_keys is not None:
        excluded_keys = [k for k in npz if k not in only_keys]
    data = {k: v for k, v in npz.items() if k not in excluded_keys and not any(kw in k for kw in excluded_keywords)}
    return data


def plot_npz(fn,
             arr_generator=lambda d: d,
             excluded_keys=(),
             only_keys=None,
             excluded_keywords=(),
             plot_generators=(),
             vmin=0, vmax=200,
             crop='target',
             maximize=True):
    suptitle = f'{fn} data'
    print(suptitle)
    data = load(fn, arr_generator=arr_generator, excluded_keys=excluded_keys, only_keys=only_keys, excluded_keywords=excluded_keywords)
    print(data.keys())
    plot_dict(data, suptitle=suptitle, vmin=vmin, vmax=vmax, crop=crop, plot_generators=plot_generators)
    if maximize:
        maximize_plots()


if __name__ == "__main__":
    import sys
    import os
    fn = " ".join(sys.argv[1:])

    params = {
        'arr_generator': lambda npz: npz, # add keys to npz dict
        'plot_generators': [ # add extra plots
            # lambda ax, npz: plot_arr(ax, 1*morphology.binary_closing(npz.get('active', np.zeros((2,2)))>0, np.ones((4,4))), 'rect'),
            # lambda ax, npz: plot_arr(ax, 1*morphology.binary_closing(npz.get('active', np.zeros((2,2)))>0, morphology.selem.disk(2)), 'A'),
            # lambda ax, npz: hist(ax, npz.get('map', np.zeros((2,2)))),
            # lambda ax, npz: plot_arr(ax, np.where(npz.get('map', np.zeros((2,2)))>95, npz.get('map',np.zeros((2,2))), 0), 'map > 95'),
            # lambda ax, npz: hist(ax, npz.get('0', np.zeros((2,2))), 'Hist 0'),
            # lambda ax, npz: hist(ax, npz.get('1', np.zeros((2,2))), 'Hist 1'),
            # lambda ax, npz: hist(ax, npz.get('2', np.zeros((2,2))), 'Hist 2'),
            # lambda ax, npz: hist(ax, npz.get('3', np.zeros((2,2))), 'Hist 3'),
            # lambda ax, npz: plot_arr(ax, np.mean(np.array([npz[str(i)] for i in range(4)]),0), 'mean'),
            # lambda ax, npz: hist(ax, np.mean(np.array([npz[str(i)] for i in range(4)]),0), 'Hist of mean'),
            # lambda ax, npz: hist(ax, npz.get('cumul_max', np.zeros((2,2))), 'Hist of cumul'),
            # lambda ax, npz: plot_arr(ax, npz.get('cumul_max', np.zeros((2,2))) > 150, 'thresh', npz)
            ], # e.g. (lambda ax, npz: hist(ax, npz['map']))
        'only_keys': None, # e.g. ['cumul_max', 'active']
        'excluded_keys': ['segments', 'target', 'mapped_pixels'], # e.g. ['segments']
        'excluded_keywords': [], # e.g. ['leak']
        'vmin': 0, # None => use min
        'vmax': None, # None => use max
        'crop': True,
    }
    print(fn)
    if fn.endswith('.npz'):
        if'_max.npz' in fn and os.path.isfile(fn.replace('_max.npz','_clean.npz')):
            clean_npz = np.load(fn.replace('_max.npz','_clean.npz'))
            clean_n = len(clean_npz.files)
            pre_clean = clean_npz['0']
            post_clean = clean_npz[str(clean_n-1)]
            params['plot_generators'].append(lambda ax, npz: plot_arr(ax, pre_clean, 'pre_clean'))
            params['plot_generators'].append(lambda ax, npz: plot_arr(ax, post_clean, 'post_clean'))
            # params['plot_generators'].append(lambda ax, npz: plot_arr(ax, pre_clean - npz['map'], 'pre_clean diff', vmax=np.nanpercentile(np.where(npz['map']  == pre_clean, np.nan, npz['map']  - pre_clean),99), vmin=-30))
            # params['plot_generators'].append(lambda ax, npz: plot_arr(ax,npz['map']  - pre_clean, 'pre_clean diff', vmax=np.nanpercentile(np.where(npz['map']  == pre_clean, np.nan, npz['map']  - pre_clean),99), vmin=0))
            params['plot_generators'].append(lambda ax, npz: plot_arr(ax,npz['map']  - pre_clean, 'pre_clean diff', vmax=None, vmin=0))
        try:
            plot_npz(fn, **params)
            plt.show(block=True)
        except Exception as e:
            print(e)
