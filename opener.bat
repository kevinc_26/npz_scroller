:: Batch script to open and plot npz files. Needed because ftype doesn't
:: want to run the python interpreter directly for some reason

:: To install, put this file and the .py script in some location it can stay forever
:: Copy the location of the python script to the command below. 

:: Open a cmd with admin privileges and use this command to associate .npzs to an ftype:
:: >assoc .npz=npzfile
:: and this command to associate the ftype to this script:
:: >ftype npzfile="C:\Users\user\Documents\PythonScripts\npz_opener\opener.bat" %0
:: !!Replace the line below with the directory of print_scroller.py on your device!!
python "C:\Users\user\OneDrive\Documents\PythonScripts\npz_opener\print_scroller.py" %*
if errorlevel 1 (
  echo Failure Reason Given is %errorlevel%
  cmd %k
)