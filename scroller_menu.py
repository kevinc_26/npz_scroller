import tkinter as tk
from tkinter.ttk import *
from tkinter import filedialog


def gui_menu(start_path=None):
    root = tk.Tk()

    root.title("Print Scroller Options")
    root.geometry('400x100')

    outputs = {}

    file_lbl = Label(root, text='Select folder')
    file_lbl.grid(column=0, row=0, sticky='W')

    def UploadAction(event=None):
        outputs['folder'] = filedialog.askdirectory(initialdir=start_path)
        file_lbl.configure(text='Folder selected, ' + outputs['folder'][0:6] + '...' + outputs['folder'][-15:])

    file_button = tk.Button(root, text='Choose folder', command=UploadAction)
    file_button.grid(column=1, row=0, sticky='W')

    # text_lbl = Label(root, text='Type only necessary for maps folders\nnot for layers')
    # text_lbl.grid(column=0, row=1, sticky='W')
    type_lbl = Label(root, text='Select map type')
    type_lbl.grid(column=0, row=2, sticky='W')
    type_combo = Combobox(root)
    type_combo['values'] = ('completed_pixels', 'cumul_max', 'first_map', 'last_map', 'stackups')
    type_combo.current(0)
    type_combo.grid(column=1, row=2, sticky='W')

    def FinishedAction():
        outputs['type'] = type_combo.get()
        root.destroy()
        print(outputs)

    finished_button = tk.Button(root, text='Finish', command=FinishedAction,
                                width=20, height=3, borderwidth=4)
    finished_button.grid(column=0, row=3)

    root.mainloop()
    return outputs


if __name__ == '__main__':
    gui_menu()