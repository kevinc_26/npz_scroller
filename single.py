import numpy as np
import matplotlib.pyplot as plt
import os.path
import glob
import ntpath

###REQUIRED###
layer_number = 4
path = r'//192.168.0.227/xilinx/mz_hw/logs/2021-08-31T16.01.45_A3099/maps/{}/'.format(layer_number)
threshold = 70
##############
string_reduction = len(str(layer_number)) + 1

npzCounter = len(glob.glob('{}{}_*_max.npz'.format(path, layer_number)))
num_maps = npzCounter - 1
print(num_maps)
max = 0
for name in glob.glob('{}{}_*_max.npz'.format(path, layer_number)):
    n = os.path.splitext(ntpath.basename(name))[0]
    n = n[string_reduction:]
    disallowed_characters = "_max"
    for character in disallowed_characters:
        n = n.replace(character, "")
    n = int(n)
    if n > max:
        max = n
print(max)
max_t = max

maps = np.array([[[0.0 for b in range(0, num_maps)] for c in range(0, 512)] for d in range(0, 480)])
cum_m = np.array([[[0.0 for b in range(0, num_maps)] for c in range(0, 512)] for d in range(0, 480)])
active = np.array([[[0.0 for b in range(0, num_maps)] for c in range(0, 512)] for d in range(0, 480)])
count = -1

for i in range(1, max_t):
    if os.path.exists('{}{}_{}_max.npz'.format(path, layer_number, i)):
        npz = np.load('{}{}_{}_max.npz'.format(path, layer_number, i))
        count = count + 1
        maps[:, :, count] = npz['map']
        cum_m[:, :, count] = npz['cumul_max']
        active[:, :, count] = npz['active']
        # plt.imshow(maps[:,:,count])
        # plt.show()
        print(count)

fig1, (ax1, ax2) = plt.subplots(1, 2)
plt.ion
plt.isinteractive
ax1.imshow(cum_m[:, :, num_maps - 1], cmap='inferno', vmin=0, vmax=200)
# plt.title('Cumulative Max')
# plt.show()

plt.waitforbuttonpress()
count = 0
loop = 2
while loop == 2:
    if plt.get_fignums():
        loop = 2
    else:
        loop = 1
        break
    pts = plt.ginput(1, timeout=-1)
    print(pts)
    y_loc = int(round(pts[0][0]))
    x_loc = int(round(pts[0][1]))
    print(x_loc)
    test_list = []
    active_list = []
    for i in range(0, num_maps):
        test_list.append(maps[x_loc, y_loc, i])
        active_list.append(active[x_loc, y_loc, i] * 100)


    # print(test_list)

    # moving average calculations
    # MA 5

    def moving_average(x, w):
        return np.convolve(x, np.ones(w), 'valid') / w


    t = np.linspace(0, num_maps, num_maps)
    threshold_vector = np.ones(num_maps) * threshold
    ax2.clear()
    ax2.set_ylim([0, 200])
    ax2.plot(t, test_list, label='Actual Map Value')
    ax2.plot(t, threshold_vector, label='Threshold')
    ax2.scatter(t, active_list, label='Active')

    average = moving_average(test_list, 7)

    t2 = np.linspace(4, num_maps, len(average))
    ax2.plot(t2, average, label='Moving Average')
    plt.legend()
    plt.xlabel('Map #')
    plt.ylabel("pixel ua")
