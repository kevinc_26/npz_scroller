import numpy as np
import matplotlib.pyplot as plt
from skimage.color import rgb2gray, rgba2rgb
import glob
import os


def npz_scroll(input_path):
    fig = plt.figure(figsize=(14,10))

    npz_0 = []
    ax_0 = fig.add_subplot(341)
    ax_0.title.set_text('0')
    npz_1 = []
    ax_1 = fig.add_subplot(342, sharex=ax_0, sharey=ax_0)
    ax_1.title.set_text('1')
    npz_0_leak = []
    ax_0_leak = fig.add_subplot(343, sharex=ax_0, sharey=ax_0)
    ax_0_leak.title.set_text('0_leak')
    npz_1_leak = []
    ax_1_leak = fig.add_subplot(344, sharex=ax_0, sharey=ax_0)
    ax_1_leak.title.set_text('1_leak')
    npz_map = []
    ax_map = fig.add_subplot(345, sharex=ax_0, sharey=ax_0)
    ax_map.title.set_text('map')
    npz_cumul_max = []
    ax_cumul_max = fig.add_subplot(346, sharex=ax_0, sharey=ax_0)
    ax_cumul_max.title.set_text('cumul_max')
    npz_initial = []
    ax_initial = fig.add_subplot(347, sharex=ax_0, sharey=ax_0)
    ax_initial.title.set_text('initial')
    npz_active = []
    ax_active = fig.add_subplot(348, sharex=ax_0, sharey=ax_0)
    ax_active.title.set_text('active')
    npz_pre_clean = []
    ax_pre_clean = fig.add_subplot(349, sharex=ax_0, sharey=ax_0)
    ax_pre_clean.title.set_text('pre clean')
    npz_post_clean = []
    ax_post_clean = fig.add_subplot(3, 4, 10, sharex=ax_0, sharey=ax_0)
    ax_post_clean.title.set_text('post clean')
    npz_histogram = []
    ax_hist = fig.add_subplot(326)
    ax_hist.title.set_text('hist')

    names = glob.glob('{}/*_max.npz'.format(input_path))
    vals = [int(x.split('_')[-2]) for x in names]
    names_sorted = [x for _,x in sorted(zip(vals, names))]

    for name in names_sorted:
        new_path = name.replace(os.sep, '/')
        print(new_path)
        try:
            npz = np.load(new_path)
            npz = {k: npz[k] for k in npz.files}

            npz_0.append(npz['0'])
            npz_1.append(npz['1'])
            npz_0_leak.append(npz['0_leak'])
            npz_1_leak.append(npz['1_leak'])
            npz_map.append(npz['map'])
            npz_cumul_max.append(npz['cumul_max'])
            npz_initial.append(npz['initial'])
            npz_active.append(npz['active'])

            npz_histogram.append(npz['map'][np.nonzero(npz['map'])])

            if os.path.isfile(new_path.replace('_max.npz', '_clean.npz')):
                clean_npz = np.load(new_path.replace('_max.npz', '_clean.npz'))
                clean_n = len(clean_npz.files)
                npz_pre_clean.append(clean_npz['0'])
                npz_post_clean.append(clean_npz[str(clean_n - 1)])
            else:
                npz_pre_clean.append(np.zeros_like(npz_0[0]))
                npz_post_clean.append(np.zeros_like(npz_0[0]))
        except NameError:
            print('Invalid npz file found')

    # plt.ion()
    """
    index = 4
    ax_0.imshow(npz_0[index], cmap='inferno', vmin=0, vmax=200)
    ax_1.imshow(npz_1[index], cmap='inferno', vmin=0, vmax=200)
    ax_0_leak.imshow(npz_0_leak[index], cmap='inferno', vmin=0, vmax=200)
    ax_1_leak.imshow(npz_1_leak[index], cmap='inferno', vmin=0, vmax=200)
    ax_map.imshow(npz_map[index], cmap='inferno', vmin=0, vmax=200)
    ax_cumul_max.imshow(npz_cumul_max[index], cmap='inferno', vmin=0, vmax=200)
    ax_initial.imshow(npz_initial[index], cmap='inferno', vmin=0, vmax=200)
    ax_active.imshow(npz_active[index], cmap='inferno', vmin=0, vmax=200)
    ax_pre_clean.imshow(npz_pre_clean[index], cmap='inferno', vmin=0, vmax=200)
    ax_post_clean.imshow(npz_post_clean[index], cmap='inferno', vmin=0, vmax=200)
    ax_hist.hist(npz_histogram[index], bins=100)
    plt.show(block=True)
    
    """
    # see https://github.com/jttoombs/CAL-Python/blob/master/Python/display_functions.py for reference

    class IndexTracker(object):
        def __init__(self):
            self.title = input_path[-10:]
            fig.suptitle('%s\nUse scroll wheel to navigate volume' % self.title)

            self.ind = 0

            self.im_0 = ax_0.imshow(npz_0[self.ind], cmap='inferno', vmin=0, vmax=200)
            self.im_1 = ax_1.imshow(npz_1[self.ind], cmap='inferno', vmin=0, vmax=200)
            self.im_0_leak = ax_0_leak.imshow(npz_0_leak[self.ind], cmap='inferno', vmin=0, vmax=200)
            self.im_1_leak = ax_1_leak.imshow(npz_1_leak[self.ind], cmap='inferno', vmin=0, vmax=200)
            self.im_map = ax_map.imshow(npz_map[self.ind], cmap='inferno', vmin=0, vmax=200)
            self.im_cumul_max = ax_cumul_max.imshow(npz_cumul_max[self.ind], cmap='inferno', vmin=0, vmax=200)
            self.im_initial = ax_initial.imshow(npz_initial[self.ind], cmap='inferno')
            self.im_active = ax_active.imshow(npz_active[self.ind], cmap='inferno')
            self.im_pre_clean = ax_pre_clean.imshow(npz_pre_clean[self.ind], cmap='inferno', vmin=0, vmax=200)
            self.im_post_clean = ax_post_clean.imshow(npz_post_clean[self.ind], cmap='inferno', vmin=0, vmax=200)
            self.im_hist = ax_hist.hist(npz_histogram[self.ind], bins=100)

            self.update()

        def onscroll(self, event):
            # print("%s %s" % (event.button, event.step))
            num_slices = len(npz_0)
            if event.button == 'up':
                self.ind = (self.ind + 1) % num_slices
            else:
                self.ind = (self.ind - 1) % num_slices
            self.update()



        def update(self):
            #print(self.ind)
            self.im_0.set_data(npz_0[self.ind])
            self.im_1.set_data(npz_1[self.ind])
            self.im_0_leak.set_data(npz_0_leak[self.ind])
            self.im_1_leak.set_data(npz_1_leak[self.ind])
            self.im_map.set_data(npz_map[self.ind])
            self.im_cumul_max.set_data(npz_cumul_max[self.ind])
            self.im_initial.set_data(npz_initial[self.ind])
            self.im_active.set_data(npz_active[self.ind])
            self.im_pre_clean.set_data(npz_pre_clean[self.ind])
            self.im_post_clean.set_data(npz_post_clean[self.ind])
            #self.im_hist.hist(npz_histogram[self.ind])
            ax_hist.cla()
            self.im_hist = ax_hist.hist(npz_histogram[self.ind], bins=100)
            ax_hist.set_xlim([0, 250])
            ax_hist.set_ylim([0, 400])
            fig.suptitle('%s\nSlice: %s' % (self.title, self.ind))

            plt.draw()

    tracker = IndexTracker()
    fig.canvas.mpl_connect('scroll_event', tracker.onscroll)
    plt.show(block=True)
